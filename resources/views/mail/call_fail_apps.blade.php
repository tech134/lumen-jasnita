<html>
  <body style="font-family: Open Sans, sans-serif; font-size: 16px; width: auto; background: #f7f7f7; padding: 10px;">
    <div style="background: white; max-width: 500px; border-top: solid 3px #ff3e00; box-shadow: 0 0 25px rgba(0,0,0,0.05); margin:auto;">
      <div class="header" style=" padding: 30px; background: #fafafa; border-bottom: solid 1px #f2f2f2">
        <img src="https://heroleads.id/assets/heroleads-logo-colored.png" style="display: block; margin: auto; width: 200px"></div>
      <div class="content" style="padding: 30px; padding-bottom: 40px;"><p style="text-align: center; margin:0;">Hi Partner,</p>
        <h2 style="font-size: 24px; text-align: center; margin-botom:20px; mso-line-height-rule:exactly;">You have missed a call from campaign: {{ $data['klien'] }} </h2>
        <table style="width: 400px; color: #999; font-size: 13px; margin: auto;"><tr>
          <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black; width: 50px;">Date</td>
          <td style="width: 5px; border-bottom: 1px solid #f7f7f7;">:</td>
          <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['endTime'] }}</td></tr>
          <tr><td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Phone</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td><td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['incomingNumber'] }}</td>
    </tr></table><br><p style="text-align:center; margin-bottom:20px;"> Do not wait until you lose your customer</p><table style="margin: auto;">
<!--<tr>
      <td>
        <a href="tel:{{ $data['incomingNumber'] }}" style="padding-left: 40px; padding-right: 40px; padding-top: 15px; padding-bottom: 15px; background: #ff3e00; color: white; text-decoration: none; text-align: center; border-radius: 100px; border: 1px solid #ff3e00; font-weight:700; display: block;">CALL NOW</a>
      </td>
    </tr> -->
      </table>
      </div>
      <br>
        @if (empty($data['apps_url']))
        <table style="margin: auto;"><tr><td>
        @php($url = "")
        <a href="{{ $url }}" style="padding-left: 40px; padding-right: 40px; padding-top: 15px; padding-bottom: 15px; background: #ff3e00; color: white; text-decoration: none; text-align: center; border-radius: 100px; border: 1px solid #ff3e00; font-weight:700; display: block;">Follow Up Now</a></td></tr>
        </table>
        @else        
        <table style="margin: auto;"><tr><td>
        <a href="{{ $data['apps_url'] }}" style="padding-left: 40px; padding-right: 40px; padding-top: 15px; padding-bottom: 15px; background: #ff3e00; color: white; text-decoration: none; text-align: center; border-radius: 100px; border: 1px solid #ff3e00; font-weight:700; display: block;">Follow Up Now</a></td></tr>
        </table>
        @endif
        <br>
      <div class="footer" style="background: black; padding: 30px; color: grey;line-height: 18px; font-size: 12px; text-align: center;">Sampoerna Stategic Square North Tower Lt. 25 - GoWork<br>Jl. Jend. Sudirman Kav. 45, Karet Semanggi,<br>Setiabudi Jakarta Selatan 12930<br>info.id@heroleads.asia | 6221 300 30 700</div>
    </div>
  </body>
</html>