<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Http\Request;

use Closure;

class RecordingdMiddleware
{
    public function handle($request, Closure $next)
    {

        // if (!($public_key == $token)) {
        //     return response()->json([
        //         'token' => "not valid"
        //     ], 401);
        // }
        
        $requestData = $request->all();

        $this->txnUuid = $requestData['txnUuid'];

        $txnRef = \DB::table('leads_call')->where('txnRef', $this->txnUuid)->first();  


        if (!$txnRef) {
        // You wil probably have some sort of helpers or whatever
        // to make sure that you have the same response format for
        // differents kind of responses. But let's return the 
        // below respose for now.
        return response()->json([
            'error' => 'txnRef is not available.'
        ], 400);
        }

        return $next($request);

    }
} 