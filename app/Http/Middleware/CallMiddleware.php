<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Http\Request;

use Closure;

class CallMiddleware
{
    public function handle($request, Closure $next)
    {
        // $request = request();
        // $token = $request->bearerToken();

        // $key = \DB::table('token')->where('id', 1)->first();

        // $public_key = $key->public_key;

        // if(!$token) {
        //     // Unauthorized response if token not there
        //     return [
        //         'code' => 401,
        //         'error' => 'Token not provided.'
        //     ];
        // }

        // if (!($public_key == $token)) {
        //     return response()->json([
        //         'token' => "not valid"
        //     ], 401);
        // }
        

        $requestData = $request->all();
        $vn = $requestData['data']['legs']['legA']['to'];
        $vn = json_encode($vn);
        $vn = json_decode($vn);

        $data_call = $requestData['data'];
        $data_call = json_encode($data_call);
        $data_call = json_decode($data_call);

        $this->txnUuid = $data_call->txnUuid;

        $data = \DB::table('virtual_number')
        ->join('campaign', 'campaign.campaign_id', '=', 'virtual_number.campaign_id')
        ->select('virtual_number.*', 'campaign.Campaign_name')
        ->where('number', $vn)
        ->first();  

        $txnRef = \DB::table('leads_call')->where('txnRef', $this->txnUuid)->first();  

        if (!$data) {
        return response()->json([
            'error' => 'Virtual number does not exist.'
        ], 400);
        }

        if (isset($txnRef)) {
        return response()->json([
            'error' => 'txnRef is exist.'
        ], 400);
        }

        return $next($request);

    }
} 