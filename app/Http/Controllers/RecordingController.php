<?php
 
namespace App\Http\Controllers;
use App\Models\Call_leads;

use App\Jobs\UploadRecording;
use App\Jobs\SendEmailCall_succ;
use App\Jobs\SendEmailCall_fail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Redirect,Response;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use File;
use Laravel\Lumen\Routing\Controller as BaseController;

class RecordingController extends BaseController 
{

            public function download(Request $request)
        {

            // $requestData = $request->all();
            $json = file_get_contents('php://input');
            $requestData = json_decode($json, true);
            var_dump($requestData);

            $this->orgUuid = $requestData['orgUuid'];
            $txnUuid = $requestData['txnUuid'];
            $this->filename = $requestData['filename'];
            $this->webhookCode = $requestData['webhookCode'];
            $this->url = "https://portal.jascloud.co.id/_o/v1/callRecordings/".$this->filename;
            $secret = "?secret=e4fe9e627f414108ab9690aeec85071c"; // secret prod
            $secret = "?secret=e4fe9e627f414108ab9690aeec85071c"; // secret staging
            $url_secret = $this->url.''.$secret;
            $posts = \DB::table('recording')->where('id', \DB::raw("(select max(id) from recording)"))->get();
            if (count($posts)){
                foreach ($posts as $data){
                $d = (int)substr($data->id,2,5);
                $d++;
                $char = "RC";
                $id = $char . sprintf("%05s", $d);
            }
            }else{
                $id = "RC00001";
            }

            try {
            // Queue::later(Carbon::now()->addMinutes(3),new UploadRecording($txnUuid, $url_secret));
                $ext = "mp3";
                $name = $txnUuid.".".$ext;
                $ch = curl_init($url_secret);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_NOBODY, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 5);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
                $output = curl_exec($ch);
                $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);          

                $path =  Storage::disk('s3')->put('file/recording/'.$name, $output);
                $this->recording = "".$txnUuid.".mp3";
                Log::info(json_encode($requestData));
            } catch (\Exception $ex) {
                Log::critical($ex);
            }

            date_default_timezone_set('Asia/Jakarta');
            $this->time = date('Y-m-d H:i:s');

            $this->validate($request, [
            // 'txnUuid' => 'required|unique:recording,txnUuid',
            'txnUuid' => 'required',
            'orgUuid' => 'required',
            ]);      

            \DB::table('recording')->insert([
                'id' => $id,
                'orgUuid' => $this->orgUuid,
                'txnUuid' => $txnUuid,
                'filename' => $this->filename,
                'webhookCode' => $this->webhookCode,
                'created_at' => $this->time,
                'updated_at' => $this->time
            ]);

            // $success = "Success";
            return response()->json($requestData, 200);
        }
}