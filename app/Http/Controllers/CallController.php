<?php
 
namespace App\Http\Controllers;
use App\Models\Call_leads;

use App\Jobs\UploadRecording;
use App\Jobs\SendEmailCall_succ;
use App\Jobs\SendEmailCall_fail;
use App\Jobs\SendEmailCall_succ_apps;
use App\Jobs\SendEmailCall_fail_apps;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Redirect,Response;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use File;
use Laravel\Lumen\Routing\Controller as BaseController;

class CallController extends BaseController 
{

public function store(Request $request)
    {

        $json = file_get_contents('php://input');
        $requestData = json_decode($json, true);
        Log::info(json_encode($requestData));

        $legA = $requestData['data']['legs']['legA'];
        $legA = json_encode($legA);
        $legA = json_decode($legA);

        $legB = $requestData['data']['legs']['legB'];
        $legB = json_encode($legB);
        $legB = json_decode($legB);

        $data_call = $requestData['data'];
        $data_call = json_encode($data_call);
        $data_call = json_decode($data_call);

        $this->status = $data_call->status;
        $this->webhookCode =  $this->status;

        // if ($this->webhookCode == "callEndedV2") {

            $this->vn = $legA->to;
            $phone_number = $legA->from;
            // $this->endpoint = $legB->to;
            $this->endpoint = $data_call->to->number;

            $this->status = $legA->status;

            $this->txnUuid = $data_call->txnUuid;
            $this->orgUuid = $data_call->orgUuid;

            $this->startTime = $data_call->startTime;
            $this->startTime = Carbon::parse($this->startTime);

            $this->endTime = $data_call->endTime;
            $this->endTime = Carbon::parse($this->endTime);

            $this->type = $data_call->type;
            $this->durationInMinutes = $data_call->durationInMinutes;
            $this->durationInSeconds = $data_call->durationInSeconds;

            $this->durationTime = (($this->durationInMinutes*60)+$this->durationInSeconds);

            // $this->time = Carbon::now('Asia/Jakarta'); 
            date_default_timezone_set('Asia/Jakarta');
            $this->time = date('Y-m-d H:i:s');

            $this->host = "https://hlid-lms.s3-ap-southeast-1.amazonaws.com/file/recording";
            $this->ext = "mp3";
            $this->file_name = $this->host."/".$this->txnUuid.".".$this->ext;

            if ($this->status == "answered") {
                $this->status = "ANSWERED";
                $this->monitorUrl = $this->file_name;
            }else{
                $this->status = "UNANSWERED";
                $this->monitorUrl = "";
            }


            $data_campaign = \DB::table('virtual_number')
            ->join('campaign', 'campaign.campaign_id', '=', 'virtual_number.campaign_id')
            ->select('virtual_number.*', 'campaign.*')
            ->where('number', $this->vn)
            ->first();  

             $this->campaign_id = $data_campaign->campaign_id;
             $this->campaign_name = $data_campaign->campaign_name;
             $this->to = $data_campaign->email_client;
             $this->cc1 = $data_campaign->cc1;
             $this->cc2 = $data_campaign->cc2;
             $this->cc3 = $data_campaign->cc3;
             $this->cc4 = $data_campaign->cc4;
             $this->herovision = $data_campaign->channel_id;
             $this->crm = $data_campaign->crm;
             $this->crm_url = $data_campaign->crm_url;
             $this->ha_apps =  $data_campaign->ha_apps;
             $this->apps_url =  $data_campaign->apps_url;
             $this->media = "LP-CALL";

            $miss_msg = "You have new Missed Call from -";
            $this->subject_miss =  $miss_msg.' '.$this->campaign_name;

            $succ_msg = "You have new Answered Call form -";
            $this->subject_succ =  $succ_msg.' '.$this->campaign_name;

            $miss_msg_apps = "You have new Missed Call from HeroAgent Apps -";
            $this->subject_miss_apps =  $miss_msg_apps.' '.$this->campaign_name;

            $succ_msg_apps = "You have new Answered Call form HeroAgent Apps -";
            $this->subject_succ_apps =  $succ_msg_apps.' '.$this->campaign_name;

            $to_mail = [$this->to, $this->cc1, $this->cc2,  $this->cc3, $this->cc4];

            $posts = \DB::table('leads_call')->where('lead_id', \DB::raw("(select max(lead_id) from leads_call)"))->get();
            if (count($posts)){
                foreach ($posts as $data){
                $d = (int)substr($data->lead_id,2,5);
                $d++;
                $char = "CL";
                $lead_id = $char . sprintf("%05s", $d);
            }
            }else{
                $lead_id = "CL00001";
            }


            $phone_number = preg_replace('/\s+/', '', $phone_number);
            $phone_number = str_replace("(","",$phone_number);
            $phone_number = str_replace(")","",$phone_number);
            $phone_number = str_replace(".","",$phone_number);
            $phone_number = str_replace("-","",$phone_number);
            $phone_number = str_replace("+","",$phone_number);
            
            if(substr(trim($phone_number), 0, 2)=='62'){
                $phone_number = substr_replace($phone_number,'0',0,2);
            }

            $this->vn = str_replace("+","",$this->vn);
            if(substr(trim($this->vn), 0, 2)=='62'){
                $this->vn = substr_replace($this->vn,'0',0,2);
            }

            $this->endpoint = str_replace("+","",$this->endpoint);
            if(substr(trim($this->endpoint), 0, 2)=='62'){
                $this->endpoint = substr_replace($this->endpoint,'0',0,2);
            }

            $phone1 = \DB::table('leads_call')->where('incomingNumber',$phone_number)->where('campaign_id',$this->campaign_id )->count();

            $phone2 = \DB::table('leads_chat')->where('phone_number',$phone_number)->where('campaign_id',$this->campaign_id )->count();

            $phone3 = \DB::table('leads_form')->where('phone_number',$phone_number)->where('campaign_id',$this->campaign_id )->count();
            
            $filter = \DB::table('leads_filter')->where('filter',$phone_number)->count();

            $this->phone = ($phone1 + $phone2 + $phone3);

            if ($this->phone > 0) {
                $this->phone = "Duplicate";
            }else{
                $this->phone = "";
            } 

            $data = new Call_leads();
            $data->lead_id = $lead_id;

            $data->txnRef = $this->txnUuid;
            $data->session = $this->orgUuid;  
            $data->transferStatus = $this->status;
            $data->campaign_id = $this->campaign_id;

            $data->incomingNumber = $phone_number;
            $data->dest = $this->endpoint;
            $data->accessNumber = $this->vn;

            // $data->client = $request->get('client');deleted
            
            $data->startTime = $this->startTime;
            $data->endTime = $this->endTime;
            
            $data->callType = $this->type;
            $data->callState = $this->webhookCode;
            
            // $data->lastInputKey = $request->get('lastInputKey');;deleted

            // $data->rate = $request->get('rate');;deleted
            // $data->debit = $request->get('debit');;deleted
            $data->durationTime = $this->durationTime;

            $data->monitorUrl = $this->monitorUrl;
            $data->media = $this->media;
            $data->created_at = $this->time;
            $data->updated_at = $this->time;
            $data->save();

            $txnRef = $this->txnUuid;

            $data_mail = [
                'to' => $to_mail,
                'klien' => $this->campaign_name,
                'startTime' => $this->startTime,
                'endTime' => $this->endTime,
                'accessNumber' => $this->vn,
                'incomingNumber' => $phone_number,
                'durationTime' => $this->durationTime,
                'txnRef' => $this->txnUuid,
                'campaign_id' => $this->campaign_id,
                'subject_succ' => $this->subject_succ,
                'subject_miss' => $this->subject_miss,
                'subject_succ_apps' => $this->subject_succ_apps,
                'subject_miss_apps' => $this->subject_miss_apps,
                'apps_url' => $this->apps_url,
            ];

            //into hero vision
            $data_herovision = [
                "timestamp" => $this->time,
                "heronumber" => $this->vn,
                "client_number" => $phone_number,
                "caller_id" => $this->endpoint,
                "duration" => $this->durationTime,
                "status" => $this->status,
                "recording_url" => $this->monitorUrl,
            ];

            //into heroagent google sheet
            $data_sheets = [
                "lead_id" => $lead_id,
                "time" => $this->time,
                "phone_number" => $phone_number,
                "recording" => $this->monitorUrl,
                "status" => $this->status,
                "channel" => $this->media,
                "endpoint" => $this->endpoint,
                "media" => $this->media,
            ];


            if ($this->herovision == "active"){
            try
            {                  
        // for productions https://leadservice.heroleads.co.th/PbxCallService?token=thioch4eimovoiDu6ahd
                    $client = new Client(
                        [
                            'base_uri' => 'https://leadservice.heroleads.co.th/',
                            'timeout'  => 10,
                            'headers' => [
                                "Content-Type" => 'application/json'
                            ]
                        ]
                    );

                $json_response =  $client->request('POST','PbxCallService?token=thioch4eimovoiDu6ahd',['json' => $data_herovision]);
                Log::info(json_encode($json_response->getBody()));
                } catch (\Exception $ex) {
                Log::critical($ex);
                }
            }


                if (($this->herovision == "inactive") AND ($this->status ==  "ANSWERED")){
                    if($this->ha_apps == "active"){
                    Queue::later(Carbon::now()->addMinutes(3),new SendEmailCall_succ_apps($data_mail));
                    }else{
                    Queue::later(Carbon::now()->addMinutes(1),new SendEmailCall_succ($data_mail));
                    }
                }else if(($this->herovision == "inactive") AND ($this->status ==  "UNANSWERED")){
                    if($this->ha_apps == "active"){
                    Queue::later(Carbon::now()->addMinutes(3),new SendEmailCall_fail_apps($data_mail));
                    }else{
                    Queue::later(Carbon::now()->addMinutes(1),new SendEmailCall_fail($data_mail));
                    }
                }

                if ($this->crm == "Google Sheets") {
                    try {
                        $client = new Client(
                            [
                                'timeout'  => 10,
                                'headers' => [
                                    "Content-Type" => 'application/json'
                                ]
                            ]
                        );
                        $json_response =  $client->request('POST', $this->crm_url,['json' => $data_sheets]);
                        Log::info(json_encode($json_response->getBody()));
                    } catch (\Exception $ex) {
                        Log::critical($ex);
                    }
                }


            $success = "Success";
            
            return response()->json($success, 200);
        }
}