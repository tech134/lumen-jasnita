<?php


namespace App\Jobs;

use App\Mail\EmailCallAnswered as EmailCallAnswered;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
// use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmailCall_succ implements ShouldQueue{

    // use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    /**
     * Create a new job instance.
     *
     * @param Carbon $currentDate
     */
    public function __construct($data)
    {
         $this->data = $data;
    }
    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle(){


        $email = new EmailCallAnswered($this->data);
        Mail::to($this->data['to'])->send($email);

    }
}