<?php


namespace App\Jobs;

// use App\Mail\EmailChat as EmailChat;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
// use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;

class UploadRecording implements ShouldQueue{

    // use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use InteractsWithQueue, Queueable, SerializesModels;

    public $txnUuid;
    public $url_secret;

    /**
     * Create a new job instance.
     *
     * @param Carbon $currentDate
     */
    public function __construct($txnUuid, $url_secret)
    {
         $this->txnUuid = $txnUuid;
         $this->url =  $url_secret ;
    }
    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle(){

        // $email = new EmailChat($this->data);
        $ext = "mp3";
        $name = $this->txnUuid.".".$ext;
        $ch = curl_init($this->url_secret);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        $output = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);          
        $path =  Storage::disk('s3')->put('file/recording/'.$name, $output);
        $this->recording = "".$this->txnUuid.".mp3";
    }
}